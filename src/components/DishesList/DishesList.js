import React from 'react';
import Dish from "../../containers/Dish/Dish";

const DishesList = (props) => {
    return (
            <div>
                {Object.keys(props.dishes).map(id => (
                    <Dish addDishOnBasket={props.addDishOnBasket}
                          key={id} image={props.dishes[id].image}
                          price={props.dishes[id].price}
                          title={props.dishes[id].title}
                          id={id}
                          deleteDish={props.deleteDish}
                          editDish={props.editDish}
                    />
                ))}
            </div>
    );
};

export default DishesList;