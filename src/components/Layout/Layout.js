import React, {Component, Fragment} from 'react';
import Toolbar from "../Navigation/Toolbar/Toolbar";
import {Container} from "reactstrap";

class Layout extends Component {
    render() {
        return (
            <Fragment>
                <Toolbar/>
                <Container>
                    {this.props.children}
                </Container>
            </Fragment>
        );
    }
}

export default Layout;