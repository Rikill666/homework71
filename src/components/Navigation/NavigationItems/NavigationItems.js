import React from 'react';
import {Nav} from "reactstrap";
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <Nav >
            <NavigationItem to="/" exact>Dishes</NavigationItem>
            <NavigationItem to="/orders" exact>Orders</NavigationItem>
        </Nav>
    );
};

export default NavigationItems;