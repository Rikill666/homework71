import React from 'react';
import {NavItem, NavLink} from "reactstrap";
import {NavLink as NavRoute} from "react-router-dom";
import './NavigatonItem.css'

const NavigationItem = (props) => {
    return (
        <NavItem>
            <NavLink tag={NavRoute} to={props.to} exact={props.exact}>{props.children}</NavLink>
        </NavItem>
    );
};

export default NavigationItem;