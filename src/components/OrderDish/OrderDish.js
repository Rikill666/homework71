import React from 'react';
import {Col, Row} from "reactstrap";

const OrderDish = (props) => {
    return (
        <li style={{"listStyleType": "none", cursor: "pointer", marginBottom: "10px"}}>
            <Row>
                <Col>
                    {props.name} x{props.count} {props.title}
                </Col>
                <Col>
                    <strong>{props.totalPrice} KGS</strong>
                </Col>

            </Row>

        </li>
    );
};

export default OrderDish;