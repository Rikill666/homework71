import React, {Component} from 'react';
import {Button, Card, CardBody, CardImg, CardText, CardTitle, Col, Row} from "reactstrap";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {deleteOrder, getOrders} from "../../store/actions/buildingOrders";

class Dish extends Component {
    async componentDidMount() {
        if(!this.props.orders)
            await this.props.getOrders();
    }

    cascadeOrdersDelete = async(dishId) => {
        this.props.deleteDish(dishId);
        for(let orderKey in this.props.orders){
            if(this.props.orders.hasOwnProperty(orderKey) && this.props.orders[orderKey].hasOwnProperty(dishId)){
                await this.props.deleteOrder(orderKey);
            }
        }
    };

    render() {
        return (
            <Card style={{marginBottom:"10px"}}>
                <Row>
                    <Col>
                        <CardImg style={{margin:"20px", width:"200px"}} top width="100%" src={this.props.image} alt={this.props.title}/>
                    </Col>
                    <CardBody>
                        <Row style={{marginTop:"30px"}}>
                            <Col>
                                <CardTitle>{this.props.title}</CardTitle>
                                <CardText>{this.props.price} KGS</CardText>
                            </Col>
                            <Col>
                                <Button tag={NavLink} to={'/dishes/edit/' + this.props.id } exact style={{marginRight:"10px"}}>Edit</Button>
                                <Button onClick={()=>this.cascadeOrdersDelete(this.props.id)}>Delete</Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Row>
            </Card>
        );
    }
}
const mapStateToProps = state => {
    return {
        orders: state.orders.orders,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        deleteOrder: (orderId) => dispatch(deleteOrder(orderId)),
        getOrders: () => dispatch(getOrders())
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(Dish);