import React, {Component} from 'react';
import OrderDish from "../../components/OrderDish/OrderDish";
import {Button, Col, Row} from "reactstrap";
import {connect} from "react-redux";
import {fetchDishes} from "../../store/actions/buildingDishes";

const DELIVERY = 150;

class OrderDishes extends Component {
    async componentDidMount() {
        if(!this.props.dishes)
        await this.props.fetchDishes();
    }

    render() {
        let orderPrice = DELIVERY;
        return (
            this.props.dishes ?
            <div style={{border: "1px solid black", marginBottom: "15px"}}>
                <Row style={{margin:"15px"}}>
                    <Col md={8} lg={8}>
                        {Object.keys(this.props.order).map(dishKey => {
                                const totalPrice = this.props.dishes[dishKey].price * this.props.order[dishKey];
                                orderPrice+=totalPrice;
                                return <OrderDish
                                    key={dishKey}
                                    count={this.props.order[dishKey]}
                                    totalPrice={totalPrice}
                                    id={dishKey}
                                    title={this.props.dishes[dishKey].title}
                                />
                        })}
                        <Row>
                            <Col>
                                Delivery:
                            </Col>
                            <Col>
                                <strong>{DELIVERY} KGS</strong>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={4} lg={4}>
                        <p>Order total:</p>
                        <p><strong>{orderPrice} KGS</strong></p>
                        <Button onClick={() => this.props.deleteOrder(this.props.orderId)}>Complete order</Button>
                    </Col>
                </Row>
            </div>: null
        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dish.dishes,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDishes: () => dispatch(fetchDishes()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderDishes);