import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {addDishOnBasket, deleteDish, fetchDishes} from "../../store/actions/buildingDishes";
import DishesList from "../../components/DishesList/DishesList";
import Spinner from "../../components/UI/Spinner/Spinner";
import {Button, Col, Row} from "reactstrap";
import {NavLink} from "react-router-dom";

class Dishes extends Component {


    componentDidMount() {
        this.props.loadingDishes();
    }

    render() {
        return (
            <Fragment>
                <Row>
                    <Col>
                        <h1>Dishes</h1>
                    </Col>
                    <Col style={{textAlign:"right"}}>
                        <Button tag={NavLink} to='/addDish' exact >Add new dish</Button>
                    </Col>
                </Row>
                <div>
                    {this.props.dishes ? this.props.loading ? <Spinner/> :
                        <DishesList dishes={this.props.dishes}
                                    loading={this.props.loading}
                                    addDishOnBasket={this.props.addDishOnBasket}
                                    deleteDish={this.props.deleteDish}
                                    editDish={this.props.editDish}
                        >
                            <Spinner/>
                        </DishesList>
                        : <h2 style={{textAlign:"center"}}>No Dishes</h2>}
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        dishes: state.dish.dishes,
        loading: state.dish.loading,
        error: state.dish.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadingDishes: () => dispatch(fetchDishes()),
        addDishOnBasket: (dishBasket) => dispatch(addDishOnBasket(dishBasket)),
        deleteDish: (dishId) => dispatch(deleteDish(dishId)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Dishes);