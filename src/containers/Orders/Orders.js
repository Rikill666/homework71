import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteOrder, getOrders} from "../../store/actions/buildingOrders";
import OrderDishes from "../OrderDishes/OrderDishes";
import Spinner from "../../components/UI/Spinner/Spinner";


class Orders extends Component {
    componentDidMount() {
        this.props.getOrders();
    }

    render() {
        return (
            this.props.orders ?
                <div
                    style={{paddingLeft: 0, paddingTop: "20px"}}
                >
                    <h1>Orders</h1>
                    {this.props.loading?<Spinner/>:
                    Object.keys(this.props.orders).map(orderKey => {
                        return <OrderDishes
                            key={orderKey}
                            orderId={orderKey}
                            order={this.props.orders[orderKey]}
                            deleteOrder={this.props.delete_Order}
                        />
                    })}
                </div> : <h1>No orders</h1>
        );
    }
}

const mapStateToProps = state => {
    return {
        orders: state.orders.orders,
        loading: state.orders.loading,
        error: state.orders.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getOrders: () => dispatch(getOrders()),
        delete_Order: (orderId) => dispatch(deleteOrder(orderId))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Orders);