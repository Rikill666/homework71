import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {editDish, fetchDish, postDish} from "../../store/actions/buildingDishes";

class AddDishes extends Component {
    state = {
        title: "",
        price: "",
        image: "",
        show: false
    };
    buttonTitle = this.props.match.params.id?"Edit":"Add";
    async componentDidMount() {
        if(this.props.match.params.id){
            await this.props.fetchDish(this.props.match.params.id);
            this.setState({title:this.props.dish.title, price:this.props.dish.price, image:this.props.dish.image});
        }
    }

    sendingNewDish = async (event) => {
        event.preventDefault();
        const dish = {
                title: this.state.title,
                price: this.state.price,
                image: this.state.image
        };
        if(this.props.match.params.id){
            await this.props.edit_Dish(this.props.match.params.id, dish, this.props.history);
        }
        else{
            await this.props.postDish(dish, this.props.history);
        }
    };
    valueChanged = event => this.setState({[event.target.name]: event.target.value});

    render() {
        return (
            <Form onSubmit={this.sendingNewDish}>
                <FormGroup>
                    <Label for="exampleTitle">Title</Label>
                    <Input type="text"
                           name="title"
                           id="exampleTitle"
                           value={this.state.title}
                           onChange={this.valueChanged}

                    />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePrice">Price</Label>
                    <Input type="number"
                           name="price"
                           id="examplePrice"
                           value={this.state.price}
                           onChange={this.valueChanged}

                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleImage">Image</Label>
                    <Input type="text"
                           name="image"
                           id="exampleImage"
                           value={this.state.image}
                           onChange={this.valueChanged}

                    />
                </FormGroup>
                <Button>{this.buttonTitle}</Button>
            </Form>
        )
    }
}
const mapStateToProps = state => {
    return {
        loading: state.dish.loading,
        error: state.dish.error,
        dish: state.dish.dish
    };
};

const mapDispatchToProps = dispatch => {
    return {
        //deleteDishOnBasket: (dishId) => dispatch(deleteDishOnBasket(dishId)),
        postDish: (dish, h) => dispatch(postDish(dish, h)),
        edit_Dish: (dishId, dish, h) => dispatch(editDish(dishId, dish, h)),
        fetchDish:(dishId) => dispatch(fetchDish(dishId))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AddDishes);