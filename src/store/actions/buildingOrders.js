import {
    DELETE_ORDER_ERROR,
    DELETE_ORDER_REQUEST,
    DELETE_ORDER_SUCCESS,
    FETCH_ORDERS_ERROR,
    FETCH_ORDERS_REQUEST,
    FETCH_ORDERS_SUCCESS,
} from "./actionsTypes";
import axiosOrder from "../../axiosOrders";

export const getOrderRequest = () => {
    return {type: FETCH_ORDERS_REQUEST};
};

export const getOrderSuccess = (orders) => {
    return {type: FETCH_ORDERS_SUCCESS, orders};
};

export const getOrderError = (error) => {
    return {type: FETCH_ORDERS_ERROR, error};
};

export const deleteOrderRequest = () => {
    return {type: DELETE_ORDER_REQUEST};
};

export const deleteOrderSuccess = () => {
    return {type: DELETE_ORDER_SUCCESS};
};

export const deleteOrderError = (error) => {
    return {type: DELETE_ORDER_ERROR, error};
};

export const deleteOrder = (orderId) => {
    return async dispatch => {
        try{
            dispatch(deleteOrderRequest());
            await axiosOrder.delete('/orders/' + orderId + '.json');
            dispatch(deleteOrderSuccess());
            dispatch(getOrders());
        }
        catch (e) {
            dispatch(deleteOrderError(e));
        }
    };
};

export const getOrders = () => {
    return async dispatch => {
        try{
            dispatch(getOrderRequest());
            const response = await axiosOrder.get('/orders.json');
            dispatch(getOrderSuccess(response.data));
        }
        catch (e) {
            dispatch(getOrderError(e));
        }
    };
};