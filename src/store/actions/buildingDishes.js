import {
    ADD_DISH_ON_BASKET,
    ADD_DISHES_ERROR,
    ADD_DISHES_REQUEST,
    ADD_DISHES_SUCCESS, DELETE_DISHES_ERROR,
    DELETE_DISHES_REQUEST,
    DELETE_DISHES_SUCCESS, FETCH_DISH_ERROR, FETCH_DISH_REQUEST, FETCH_DISH_SUCCESS,
    FETCH_DISHES_ERROR,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS, PUT_DISHES_ERROR, PUT_DISHES_REQUEST, PUT_DISHES_SUCCESS,
} from "./actionsTypes";
import axiosOrder from "../../axiosOrders";

export const dishesRequest = () => {
    return {type: FETCH_DISHES_REQUEST};
};

export const dishesSuccess = (dishes) => {
    return {type: FETCH_DISHES_SUCCESS, dishes};
};

export const dishesError = (error) => {
    return {type: FETCH_DISHES_ERROR, error};
};

export const dishRequest = () => {
    return {type: FETCH_DISH_REQUEST};
};

export const dishSuccess = (dish) => {
    return {type: FETCH_DISH_SUCCESS, dish};
};

export const dishError = (error) => {
    return {type: FETCH_DISH_ERROR, error};
};

export const addDishRequest = () => {
    return {type: ADD_DISHES_REQUEST};
};

export const addDishSuccess = () => {
    return {type: ADD_DISHES_SUCCESS};
};

export const addDishError = (error) => {
    return {type: ADD_DISHES_ERROR, error};
};

export const deleteDishRequest = () => {
    return {type: DELETE_DISHES_REQUEST};
};

export const deleteDishSuccess = () => {
    return {type: DELETE_DISHES_SUCCESS};
};

export const deleteDishError = (error) => {
    return {type: DELETE_DISHES_ERROR, error};
};

export const editDishRequest = () => {
    return {type: PUT_DISHES_REQUEST};
};

export const editDishSuccess = () => {
    return {type: PUT_DISHES_SUCCESS};
};

export const editDishError = (error) => {
    return {type: PUT_DISHES_ERROR, error};
};

export const editDish = (dishId, dish, history) => {
    return async dispatch => {
        try{
            dispatch(editDishRequest());
            await axiosOrder.put('/dishes/'+ dishId +'.json', dish);
            dispatch(editDishSuccess());
            history.push("/");
        }
        catch (e) {
            dispatch(editDishError(e));
        }
    };
};

export const addDishOnBasket = (dishBasket) => {
    return {type: ADD_DISH_ON_BASKET, dishBasket};
};


export const deleteDish = (dishId) => {
    return async dispatch => {
        try{
            dispatch(deleteDishRequest());
            await axiosOrder.delete('/dishes/'+ dishId + ".json");
            dispatch(deleteDishSuccess());
            dispatch(fetchDishes());
        }
        catch (e) {
            dispatch(deleteDishError(e));
        }
    };
};

export const postDish = (dish, history) => {
    return async dispatch => {
        try{
            dispatch(addDishRequest());
            await axiosOrder.post('/dishes.json', dish);
            dispatch(addDishSuccess());
            history.push("/");
        }
        catch (e) {
            dispatch(addDishError(e));
        }
    };
};


export const fetchDishes = () => {
    return async dispatch => {
        try{
            dispatch(dishesRequest());
            const response = await axiosOrder.get('/dishes.json');
            dispatch(dishesSuccess(response.data));
        }
        catch (e) {
            dispatch(dishesError(e));
        }
    };
};

export const fetchDish = (dishId) => {
    return async dispatch => {
        try{
            dispatch(dishRequest());
            const response = await axiosOrder.get('/dishes/' + dishId +'.json');
            dispatch(dishSuccess(response.data));
        }
        catch (e) {
            dispatch(dishError(e));
        }
    };
};