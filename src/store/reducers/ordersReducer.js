import {
    DELETE_ORDER_ERROR, DELETE_ORDER_REQUEST, DELETE_ORDER_SUCCESS,
    FETCH_ORDERS_ERROR,
    FETCH_ORDERS_REQUEST,
    FETCH_ORDERS_SUCCESS,
} from "../actions/actionsTypes";

const initialState = {
    orders: null,
    loading: false,
    error: null,
};
const ordersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDERS_REQUEST:
            return {...state, loading: true};
        case FETCH_ORDERS_SUCCESS:
            return {...state, error: null, orders: action.orders, loading: false};
        case FETCH_ORDERS_ERROR:
            return {...state, error: action.error, loading: true};

        case DELETE_ORDER_REQUEST:
            return {...state, loading: true};
        case DELETE_ORDER_SUCCESS:
            return {...state, error: null, loading: false};
        case DELETE_ORDER_ERROR:
            return {...state, error: action.error, loading: true};
        default:
            return state;
    }
};


export default ordersReducer;