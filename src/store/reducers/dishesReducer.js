import {
    ADD_DISHES_ERROR,
    ADD_DISHES_REQUEST,
    ADD_DISHES_SUCCESS,
    DELETE_DISHES_ERROR,
    DELETE_DISHES_REQUEST,
    DELETE_DISHES_SUCCESS,
    FETCH_DISH_ERROR,
    FETCH_DISH_REQUEST,
    FETCH_DISH_SUCCESS,
    FETCH_DISHES_ERROR,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
} from "../actions/actionsTypes";

const initialState = {
    dishes: null,
    error: null,
    loading: false,
    dish:{}
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {...state, loading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, dishes: action.dishes,error:null, loading: false};
        case FETCH_DISHES_ERROR:
            return {...state,error:action.error, loading: true};

        case FETCH_DISH_REQUEST:
            return {...state, loading: true};
        case FETCH_DISH_SUCCESS:
            return {...state, dish: action.dish, error:null, loading: false};
        case FETCH_DISH_ERROR:
            return {...state,error:action.error, loading: true};

        case ADD_DISHES_REQUEST:
            return {...state, loading: true};
        case ADD_DISHES_SUCCESS:
            return {...state, error: null, loading: false};
        case ADD_DISHES_ERROR:
            return {...state, error: action.error, loading: true};

        case DELETE_DISHES_REQUEST:
            return {...state, loading: true};
        case DELETE_DISHES_SUCCESS:
            return {...state, error: null, loading: false};
        case DELETE_DISHES_ERROR:
            return {...state, error: action.error, loading: true};
        default:
            return state;
    }
};



export default dishesReducer;