import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Container} from "reactstrap";
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";
import Dishes from "./containers/Dishes/Dishes";
import AddDishes from "./containers/AddDishes/AddDishes";


const App = () => {
    return (
        <Container>
            <Layout>
                <Switch>
                    <Route path="/" exact component={Dishes}/>
                    <Route path="/addDish" exact component={AddDishes}/>
                    <Route path="/dishes/edit/:id" exact component={AddDishes}/>
                    <Route path="/orders" exact component={Orders}/>
                    <Route render={() => <h1>Not Found</h1>}/>
                </Switch>
            </Layout>
        </Container>
    );
};

export default App;